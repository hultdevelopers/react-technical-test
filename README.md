# Hult React Technical Test

### For this test you have to complete the following:
- Build a overview/results page, that can filter courses data on course type

### Resources

The following resources can be found in the **"resources"** folder on the project root.

- Course data (JSON)
- Hult Brand Colours
- Desktop & Mobile UI Design
- Hult Logo

### Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Optional

Update the project to Typescript.

Create a types.d.ts to store required Types. (i.e. Course Data Type)


You can use https://github.com/google/gts to create the base configuration for the TypeScript setup by running the command `npx gts init`.